//
//  AZInfoVC.h
//  AZ Medical Information App
//
//  Created by Nik Lever on 19/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZInfoBVC : UIViewController<UITextViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *name_txt;
@property (weak, nonatomic) IBOutlet UITextField *country_txt;
@property (weak, nonatomic) IBOutlet UITextField *email_txt;
@property (weak, nonatomic) IBOutlet UIImageView *name_img;
@property (weak, nonatomic) IBOutlet UIImageView *country_img;
@property (weak, nonatomic) IBOutlet UIImageView *email_img;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity_vw;
@property (weak, nonatomic) IBOutlet UILabel *info_txt;

- (IBAction)continuePressed:(id)sender;
@end
