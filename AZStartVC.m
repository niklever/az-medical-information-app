//
//  AZStartVC.m
//  AZ Medical Information App
//
//  Created by Nik Lever on 19/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import "AZStartVC.h"
#import "AZDisclaimerVC.h"
#import "AZInfoVC.h"

@interface AZStartVC ()

@end

@implementation AZStartVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UISwipeGestureRecognizer *leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
    leftRecognizer.direction=UISwipeGestureRecognizerDirectionLeft;
    leftRecognizer.numberOfTouchesRequired = 1;
    leftRecognizer.delegate = self;
    [self.view addGestureRecognizer:leftRecognizer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) SwipeRecognizer:(UISwipeGestureRecognizer *)sender {
    //AZDisclaimerVC *vc = [[AZDisclaimerVC alloc] initWithNibName:@"AZDisclaimerVC" bundle:nil];
    AZInfoVC *vc = [[AZInfoVC alloc] initWithNibName:@"AZInfoVC" bundle:nil];
    CGRect frame = self.view.frame;
    frame.origin.x = -1024;
    [UIView animateWithDuration: 0.5
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.view.frame = frame;
                     }
                     completion: ^(BOOL finished) {
                         [self.navigationController popViewControllerAnimated:NO];
                         [self.navigationController pushViewController:vc animated:YES];
                     }];
}

@end
