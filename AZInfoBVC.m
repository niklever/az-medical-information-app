//
//  AZInfoVC.m
//  AZ Medical Information App
//
//  Created by Nik Lever on 19/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import "AZInfoBVC.h"
#import "AZDetailsVC.h"

enum { _PICKER_TITLE, _PICKER_COUNTRIES };

@interface AZInfoBVC (){
     UITapGestureRecognizer *tapper;
     NSString *info;
    NSTimer *error_tmr;
}

@end

@implementation AZInfoBVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
     }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    _activity_vw.hidden = YES;
    _info_txt.hidden = YES;
    //[self populate];
}

- (void)populate{
    _name_txt.text = @"Nik Lever";
    _email_txt.text = @"nik@catalystpics.co.uk";
    _country_txt.text = @"UK";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)hideKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
 }

- (void)showError:(NSString *)str view:(UIView *)vw{
    if (error_tmr!=nil) [error_tmr invalidate];
    error_tmr = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hideError) userInfo:nil repeats:NO];
    [vw glowANumberOfTimes:2];
    _info_txt.text = str;
    _info_txt.hidden = NO;
}

- (void)hideError{
    error_tmr = nil;
    _info_txt.text = info;
}

- (BOOL)checkEmail:(NSString *)email{
    BOOL at = ([email rangeOfString:@"@"].location != NSNotFound);
    BOOL dot = ([email rangeOfString:@"."].location != NSNotFound);
    return (at && dot);
}

- (IBAction)continuePressed:(id)sender {
    [self.view endEditing:YES];
    
    if ([_name_txt.text isEqualToString:@""]){
        [self showError:@"Please enter your name" view:_name_img];
    }else if ([_email_txt.text isEqualToString:@""]){
        [self showError:@"Please enter your email" view:_email_img];
    }else if (![self checkEmail:_email_txt.text]){
        [self showError:@"Please check your email address" view:_email_img];
    }else if ([_country_txt.text isEqualToString:@""]){
        [self showError:@"Please enter your country" view:_country_img];
    }else{
        //http://hosted.uk.com/az/ipad/?title=Mr&fullname=Terry%20temp&degree=DEgreee&institute=institute&address=address&country=Sweden&email=myemail@domain.com&phone=123456&affiliation=az&details=My%20details&azflag=TEMP
        azApp.user_data = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"notused", @"title", _name_txt.text, @"fullname", _email_txt.text, @"email", _country_txt.text, @"address", @"az", @"affiliation", nil];
        AZDetailsVC *vc = [[AZDetailsVC alloc] initWithNibName:@"AZDetailsVC" bundle:nil];
        CGRect frame = self.view.frame;
        frame.origin.x = -1024;
        [UIView animateWithDuration: 0.5
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.view.frame = frame;
                         }
                         completion: ^(BOOL finished) {
                             [self.navigationController pushViewController:vc animated:YES];
                             //[self.navigationController popViewControllerAnimated:NO];
                         }];
        //_activity_vw.hidden = NO;
        //[_activity_vw startAnimating];
        //_info_txt.text = @"Sending your details ...";
        
        //[azApp.webservice saveUserData:azApp.user_data delegate:self];
    }
    
    NSLog(@"Continue pressed");
}

/*-(void)webServiceOnComplete:(NSDictionary *)dict{
    NSLog(@"webServiceOnComplete %@", dict);
    
    BOOL success = [[dict objectForKey:@"success"] boolValue];
    
    if (success){
        AZThanksVC *vc = [[AZThanksVC alloc] initWithNibName:@"AZThanksVC" bundle:nil];
        CGRect frame = self.view.frame;
        frame.origin.x = -1024;
        [UIView animateWithDuration: 0.5
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.view.frame = frame;
                         }
                         completion: ^(BOOL finished) {
                             [self.navigationController pushViewController:vc animated:YES];
                             //[self.navigationController popViewControllerAnimated:NO];
                         }];
        
    }else{
        _activity_vw.hidden = YES;
        [_activity_vw stopAnimating];
        _info_txt.text = @"There was a problem sending your details please try again.";
    }
}*/

-(void)textViewDidChange:(UITextView *)textView{
    //if (textView == _address_tvw){
       // _address_lbl.hidden = (textView.text.length!=0);
    //}else if (textView == _question_tvw){
   //     _question_lbl.hidden = (textView.text.length!=0);
    //}
}

- (void)textFieldDidBeginEditing:(UITextField *)textView{
    NSLog(@"textFieldDidBeginEditing");
    //_picker_vw.hidden = _picker_img.hidden = YES;
    _info_txt.hidden = YES;
    
}

- (void)textFieldDidChange:(UITextField *)txtView{
    
}

- (void)textFieldDidEndEditing:(UITextField *)txtView{
    
}

@end
