//
//  AZDetailsVC.m
//  AZ Medical Information App
//
//  Created by Nik Lever on 20/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import "AZDetailsVC.h"
#import "AZThanksVC.h"

@interface AZDetailsVC (){
    UITapGestureRecognizer *tapper;
    NSString *info;
}

@end

@implementation AZDetailsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    info = _info_txt.text;
    _activity_vw.hidden = YES;
}

- (void)hideKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    NSLog(@"textViewDidBeginEditing");
}

- (void)textViewDidChange:(UITextView *)txtView{
    _details_lbl.hidden = ([txtView.text length] > 0);
}

- (void)textViewDidEndEditing:(UITextView *)txtView{
    _details_lbl.hidden = ([txtView.text length] > 0);
}

- (IBAction)submitPressed:(id)sender {
    _activity_vw.hidden = NO;
    [_activity_vw startAnimating];
    _info_txt.text = @"Sending your details ...";
    _details_img.hidden = YES;
    _details_txt.hidden = YES;
    _details_lbl.hidden = YES;
    _submit_btn.hidden = YES;
    
    [azApp.user_data setObject:_details_txt.text forKey:@"details"];
    [azApp.user_data setObject:@"ECCMID2015" forKey:@"azflag"];
    [azApp.webservice saveUserData:azApp.user_data delegate:self];
}

-(void)webServiceOnComplete:(NSDictionary *)dict{
    NSLog(@"webServiceOnComplete %@", dict);
    
    BOOL success = [[dict objectForKey:@"success"] boolValue];
    
    if (success){
        AZThanksVC *vc = [[AZThanksVC alloc] initWithNibName:@"AZThanksVC" bundle:nil];
        CGRect frame = self.view.frame;
        frame.origin.x = -1024;
        [UIView animateWithDuration: 0.5
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.view.frame = frame;
                         }
                         completion: ^(BOOL finished) {
                             [self.navigationController pushViewController:vc animated:YES];
                             //[self.navigationController popViewControllerAnimated:NO];
                         }];

    }else{
        _activity_vw.hidden = YES;
        [_activity_vw stopAnimating];
        _info_txt.text = @"There was a problem sending your details please try again.";
        _details_img.hidden = NO;
        _details_txt.hidden = NO;
        _details_lbl.hidden = NO;
        _submit_btn.hidden = NO;
    }
}

@end
