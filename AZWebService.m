//
//  AZWebService.m
//  AZ Medical Information App
//
//  Created by Nik Lever on 20/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import "AZWebService.h"

@implementation AZWebService

@synthesize delegate;

-(MKNetworkOperation*) saveUserData:(NSDictionary *)dict delegate:(id)_delegate{
    NSString *str = @"";
    NSString *url = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"saveUserData %@", url);
    
    self.delegate = _delegate;
    
    MKNetworkOperation *op = [self operationWithPath:url
                                              params:dict
                                          httpMethod:@"GET"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         if(![completedOperation isCachedResponse]) {
             NSLog(@"saveUserData data from server %@", [completedOperation responseString]);
             NSDictionary *dict = [self dictionaryFromString:[completedOperation responseString]];
             if (delegate!=nil) [delegate webServiceOnComplete:dict];
         }
     }errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
         DLog(@"%@", [error localizedDescription]);
         NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:false], @"success", [error localizedDescription], @"msg", nil];
         if (delegate!=nil) [delegate webServiceOnComplete:dict];
     }];
    
    [self enqueueOperation:op];
    
    return op;
}

-(NSDictionary*)dictionaryFromString:(NSString*)str{
    NSError* error;
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data
                          options:kNilOptions
                          error:&error];
    if (error != nil){
        json = [NSDictionary dictionaryWithObjectsAndKeys:false, @"success", [error localizedDescription], @"msg", nil];
    }
    
    return json;
}

@end
