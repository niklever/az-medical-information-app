//
//  AZStartVC.h
//  AZ Medical Information App
//
//  Created by Nik Lever on 19/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZStartVC : UIViewController<UIGestureRecognizerDelegate>

@end
