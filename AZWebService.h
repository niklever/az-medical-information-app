//
//  AZWebService.h
//  AZ Medical Information App
//
//  Created by Nik Lever on 20/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import "MKNetworkEngine.h"

@protocol AZWebServiceDelegate <NSObject>
- (void)webServiceOnComplete:(NSDictionary*)dict;
@end

@interface AZWebService : MKNetworkEngine
@property (strong, nonatomic) id delegate;

- (MKNetworkOperation*) saveUserData:(NSDictionary *)dict delegate:(id)_delegate;

@end
