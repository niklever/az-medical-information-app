//
//  AZInfoVC.h
//  AZ Medical Information App
//
//  Created by Nik Lever on 19/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZInfoVC : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, UITextViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *title_txt;
@property (weak, nonatomic) IBOutlet UITextField *country_txt;
@property (weak, nonatomic) IBOutlet UITextField *fullname_txt;
@property (weak, nonatomic) IBOutlet UITextField *email_txt;
@property (weak, nonatomic) IBOutlet UITextField *affiliation_txt;
@property (weak, nonatomic) IBOutlet UIPickerView *picker_vw;
@property (weak, nonatomic) IBOutlet UIImageView *title_img;
@property (weak, nonatomic) IBOutlet UIImageView *country_img;
@property (weak, nonatomic) IBOutlet UIImageView *email_img;
@property (weak, nonatomic) IBOutlet UIImageView *name_img;
@property (weak, nonatomic) IBOutlet UIImageView *affliation_img;
@property (weak, nonatomic) IBOutlet UILabel *info_txt;
@property (weak, nonatomic) IBOutlet UIImageView *picker_img;

- (IBAction)titlePressed:(id)sender;
- (IBAction)countryPressed:(id)sender;
- (IBAction)continuePressed:(id)sender;
@end
