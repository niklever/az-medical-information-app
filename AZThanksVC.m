//
//  AZThanksVC.m
//  AZ Medical Information App
//
//  Created by Nik Lever on 20/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import "AZThanksVC.h"

@interface AZThanksVC ()

@end

@implementation AZThanksVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated{
    NSTimer *tmr = [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(endVC) userInfo:nil repeats:NO];
}

- (void)endVC{
    [azApp returnToStart];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
