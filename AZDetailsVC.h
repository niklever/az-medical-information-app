//
//  AZDetailsVC.h
//  AZ Medical Information App
//
//  Created by Nik Lever on 20/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AZWebService.h"

@interface AZDetailsVC : UIViewController<UITextViewDelegate, AZWebServiceDelegate>
@property (weak, nonatomic) IBOutlet UITextView *details_txt;
@property (weak, nonatomic) IBOutlet UILabel *details_lbl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity_vw;
@property (weak, nonatomic) IBOutlet UILabel *info_txt;
@property (weak, nonatomic) IBOutlet UIImageView *details_img;
@property (weak, nonatomic) IBOutlet UIButton *submit_btn;

- (IBAction)submitPressed:(id)sender;
@end
