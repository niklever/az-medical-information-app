//
//  AZDisclaimerVC.m
//  AZ Medical Information App
//
//  Created by Nik Lever on 02/04/2015.
//  Copyright (c) 2015 Nik Lever. All rights reserved.
//

#import "AZDisclaimerVC.h"
#import "AZInfoVC.h"

@interface AZDisclaimerVC ()

@end

@implementation AZDisclaimerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)acceptPressed:(id)sender {
    AZInfoVC *vc = [[AZInfoVC alloc] initWithNibName:@"AZInfoVC" bundle:nil];
    CGRect frame = self.view.frame;
    frame.origin.x = -1024;
    [UIView animateWithDuration: 0.5
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.view.frame = frame;
                     }
                     completion: ^(BOOL finished) {
                         //[self.navigationController popViewControllerAnimated:NO];
                         [self.navigationController pushViewController:vc animated:YES];
                     }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
