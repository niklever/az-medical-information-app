//
//  AZInfoVC.m
//  AZ Medical Information App
//
//  Created by Nik Lever on 19/08/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import "AZInfoVC.h"
#import "AZDetailsVC.h"

enum { _PICKER_TITLE, _PICKER_COUNTRIES };

@interface AZInfoVC (){
    NSArray *countries;
    NSArray *titles;
    UITapGestureRecognizer *tapper;
    int pickerMode;
    NSString *info;
    NSTimer *error_tmr;
}

@end

@implementation AZInfoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        countries = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"country" ofType:@"plist"]];
        titles = [[NSArray alloc] initWithObjects:@"", @"Mr", @"Mrs", @"Miss", @"Dr", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    info = _info_txt.text;
    
    //[self populate];
}

- (void)populate{
    _title_txt.text = @"Mr";
    _country_txt.text = @"UK";
    _fullname_txt.text = @"Nik Lever";
    _email_txt.text = @"nik@catalystpics.co.uk";
    _affiliation_txt.text = @"Sailing";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)hideKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
    _picker_vw.hidden = _picker_img.hidden = YES;
}

- (void)showError:(NSString *)str view:(UIView *)vw{
    if (error_tmr!=nil) [error_tmr invalidate];
    error_tmr = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hideError) userInfo:nil repeats:NO];
    [vw glowANumberOfTimes:2];
    _info_txt.text = str;
}

- (void)hideError{
    error_tmr = nil;
    _info_txt.text = info;
}

- (IBAction)titlePressed:(id)sender {
    pickerMode = _PICKER_TITLE;
    [_picker_vw reloadAllComponents];
    [_picker_vw selectRow:0 inComponent:0 animated:NO];
    _picker_vw.hidden = _picker_img.hidden = NO;
}

- (IBAction)countryPressed:(id)sender {
    pickerMode = _PICKER_COUNTRIES;
    [_picker_vw reloadAllComponents];
    [_picker_vw selectRow:0 inComponent:0 animated:NO];
    _picker_vw.hidden = _picker_img.hidden = NO;
}

- (BOOL)checkEmail:(NSString *)email{
    BOOL at = ([email rangeOfString:@"@"].location != NSNotFound);
    BOOL dot = ([email rangeOfString:@"."].location != NSNotFound);
    return (at && dot);
}

- (IBAction)continuePressed:(id)sender {
    [self.view endEditing:YES];
    _picker_vw.hidden = _picker_img.hidden = YES;
    
    if ([_title_txt.text isEqualToString:@""]){
        [self showError:@"Please choose a title" view:_title_img];
    }else if ([_country_txt.text isEqualToString:@""]){
        [self showError:@"Please choose a country" view:_country_img];
    }else if ([_fullname_txt.text isEqualToString:@""]){
        [self showError:@"Please enter your name" view:_name_img];
    }else if ([_email_txt.text isEqualToString:@""]){
        [self showError:@"Please enter your email" view:_email_img];
    }else if (![self checkEmail:_email_txt.text]){
        [self showError:@"Please check your email address" view:_email_img];
    }else{
        azApp.user_data = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_title_txt.text, @"title", _country_txt.text, @"country", _fullname_txt.text, @"fullname", _email_txt.text, @"email", _affiliation_txt.text, @"affiliation", nil];
        AZDetailsVC *vc = [[AZDetailsVC alloc] initWithNibName:@"AZDetailsVC" bundle:nil];
        CGRect frame = self.view.frame;
        frame.origin.x = -1024;
        [UIView animateWithDuration: 0.5
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.view.frame = frame;
                         }
                         completion: ^(BOOL finished) {
                             [self.navigationController pushViewController:vc animated:YES];
                             //[self.navigationController popViewControllerAnimated:NO];
                         }];

    }
    
    NSLog(@"Continue pressed");
}

- (void)textFieldDidBeginEditing:(UITextView *)textView{
    NSLog(@"textFieldDidBeginEditing");
    _picker_vw.hidden = _picker_img.hidden = YES;
}

- (void)textFieldDidChange:(UITextView *)txtView{
}

- (void)textFieldDidEndEditing:(UITextView *)txtView{
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch(pickerMode){
        case _PICKER_COUNTRIES:
            _country_txt.text = [countries objectAtIndex:row];
            break;
        case _PICKER_TITLE:
            _title_txt.text = [titles objectAtIndex:row];
            break;
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    int result = 0;
    switch(pickerMode){
        case _PICKER_COUNTRIES:
            result = [countries count];
            break;
        case _PICKER_TITLE:
            result = [titles count];
            break;
    }
    return result;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *str = @"";
    
    switch(pickerMode){
        case _PICKER_COUNTRIES:
            str = [countries objectAtIndex:row];
            break;
        case _PICKER_TITLE:
            str = [titles objectAtIndex:row];
            break;
    }

    return str;
}

@end
