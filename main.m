//
//  main.m
//  AZ Medical Information App
//
//  Created by Nik Lever on 23/04/2015.
//  Copyright (c) 2015 Nik Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AZAppDelegate class]));
    }
}
