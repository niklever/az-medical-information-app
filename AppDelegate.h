//
//  AppDelegate.h
//  AZ Medical Information App
//
//  Created by Nik Lever on 23/04/2015.
//  Copyright (c) 2015 Nik Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <UIKit/UIKit.h>
#import "AZWebService.h"

@interface AZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) NSMutableDictionary *user_data;
@property (strong, nonatomic) AZWebService *webservice;

-(void)returnToStart;


@end

